const SearchResult = (props) => {
    const { value, setKeyword } = props;

    return (
        <p className="search-item" onClick={() => setKeyword(value)}>{value}</p>
    )
};

export default SearchResult;