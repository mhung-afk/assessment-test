import { ENDPOINT, REST_API_WEB_SERVICE, UPDATE_OPT } from '../constrain/constain';

const UpdateWord = (props) => {
    const { replacementOpt, setReplacementOpt, alterWord, setAlterWord, fetchReplaceWord } = props;
    const openResource = () => {
        window.open(REST_API_WEB_SERVICE+ENDPOINT.SEARCH+'/corpus', '_blank');
    }

    return (
        <>
            <div className='inp-form'>
                <label>Alternative word</label>
                <input className='alter-text' placeholder='Ex: Canada, etc.' type="text" value={alterWord} onChange={e => setAlterWord(e.target.value)} /> <br />
            </div>

            <div className='inp-form'>
                <label>Replacement option</label>
                <div className='block-radio'>
                    <input type="radio" id="selectedWord" name="optUpdating" onChange={e => setReplacementOpt(e.target.checked ? UPDATE_OPT.SELECTED : UPDATE_OPT.SIMILAR)} value={UPDATE_OPT.SELECTED} checked={replacementOpt === UPDATE_OPT.SELECTED} />
                    <label className='label-radio' htmlFor="selectedWord">Update for the selected word</label><br />

                    <input type="radio" id="similarWord" name="optUpdating" onChange={e => setReplacementOpt(e.target.checked ? UPDATE_OPT.SIMILAR : UPDATE_OPT.SELECTED)} value={UPDATE_OPT.SIMILAR} checked={replacementOpt === UPDATE_OPT.SIMILAR} />
                    <label className='label-radio' htmlFor="similarWord">Update for the most similar word</label>
                </div>
            </div>

            <div className='btn'>
                <button onClick={fetchReplaceWord}>Replace</button>
                <button onClick={openResource} >Check the corpus</button>
            </div>
        </>
    )
};

export default UpdateWord;