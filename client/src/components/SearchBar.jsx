import { useEffect, useState } from 'react';
import { searchEndpoint } from '../constrain/api';
import { UPDATE_OPT } from '../constrain/constain';
import SearchResult from './SearchResult'
import UpdateWord from './UpdateWord';
import { ImSearch } from 'react-icons/im';

const SearchBar = () => {
    const [keyword, setKeyword] = useState('');
    const [foundWords, setFoundWords] = useState([]);
    const [replacementOpt, setReplacementOpt] = useState(UPDATE_OPT.SIMILAR);
    const [alterWord, setAlterWord] = useState('');

    const fetchSearchAPI = async () => {
        try {
            const res = await searchEndpoint.getWordsByKeyword(keyword);
            if (res.status === 400) throw res.data;
            setFoundWords(res.data.data);
        } catch (error) {
            alert(error.message);
        }
    }

    const fetchReplaceWord = async () => {
        try {
            const res = await searchEndpoint.replaceWord(alterWord, replacementOpt === UPDATE_OPT.SELECTED ? keyword : null);
            if (res.status === 400) throw res.data;
            setKeyword(res.data.data.newWord);
            fetchSearchAPI();
        } catch (error) {
            alert(error.message);
        }
    }

    useEffect(() => {
        if (keyword.length > 0)
            fetchSearchAPI();
        else
            setFoundWords([]);
    }, [keyword])

    return (
        <div className='search-container'>
            <div className='split left'>
                <div id='search-box'>
                    <input id='search-bar' value={keyword} type="text" placeholder="Ex: Louis, etc." onChange={e => setKeyword(e.target.value)} />
                    <div id='search-icon'>
                        <ImSearch />
                    </div>
                </div>

                <div id='search-result'>
                    <p>The {foundWords.length} most similar words:</p>
                    {foundWords.length > 0 ?
                        foundWords.map(val => <SearchResult value={val} keyword={keyword} setKeyword={setKeyword} />)
                        :
                        <></>
                    }
                </div>
            </div>
            <div className='split right'>
                <UpdateWord fetchReplaceWord={fetchReplaceWord} replacementOpt={replacementOpt} setReplacementOpt={setReplacementOpt} alterWord={alterWord} setAlterWord={setAlterWord} />
            </div>
        </div>
    )
};

export default SearchBar;