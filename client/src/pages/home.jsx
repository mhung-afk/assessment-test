import Searchbar from "../components/SearchBar";

const Home = () => {
    return (
        <>
            <Searchbar />
        </>
    )
};

export default Home;