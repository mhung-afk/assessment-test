import './App.css';
import { Home } from './pages';

function App() {
  return (
    <>
      <div class="shadow">Test Submission</div>
      <Home />
      <p className='footer'>Implemented by Henry Le</p>
    </>
  );
}

export default App;
