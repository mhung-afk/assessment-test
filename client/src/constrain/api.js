import axios from 'axios';
import { REST_API_WEB_SERVICE, ENDPOINT } from './constain'

export const searchEndpoint = {
    getWordsByKeyword: async keyword => {
        try {
            const res = await axios.get(REST_API_WEB_SERVICE+ENDPOINT.SEARCH, {
                params: {
                    keyword
                }
            });
            return res;
        } catch (error) {
            // console.log(error.response);
            return error.response;
        }
    },
    replaceWord: async (given, selected = null) => {
        try {
            const res = await axios.put(REST_API_WEB_SERVICE+ENDPOINT.SEARCH, {
                givenWord: given,
                selected
            });
            return res;
        } catch (error) {
            // console.log(error.response);
            return error.response;
        }
    }
}