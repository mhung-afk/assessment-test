export const REST_API_WEB_SERVICE = "http://localhost:8000/api/v1";

export const ENDPOINT = {
    SEARCH: '/search'
};

export const UPDATE_OPT = {
    SELECTED: 'selected',
    SIMILAR: 'similar'
}