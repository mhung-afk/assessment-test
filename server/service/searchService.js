import { listAllWords, readFile, replaceAll, removeListOfElements, writeFile } from "../helper/utils.js";

/**
 * search words in supplied file in descending priority with 3 criteria:
 * 1. the word matches the keyword fully
 * 2. the word matches the keyword fully but it doesn't care lowercase/uppercase characters
 * 3. the word includes the keyword
 * 4. the word matches a part of the keyword
 * 5. a part of the word matches a part of the keyword
 * @param {*} keyword 
 * @param {string} fileName 
 * @returns {string[]} return similar words
 */
export async function search(keyword, fileName) {
    try {
        const data = (await readFile(fileName)).toString();
        let words = listAllWords(data);

        let searchResult = [];

        // criteria 1
        if (words.includes(keyword)) {
            searchResult.push(keyword);
            words = words.filter(val => val !== keyword);
        }

        // criteria 2
        if (searchResult.length < 3) {
            const matchWords = words.filter(val => val.toLowerCase() === keyword.toLowerCase());
            searchResult.push(...matchWords);
            words = removeListOfElements(words, ...matchWords);
        }

        // criteria 3
        if (searchResult.length < 3) {
            const matchWords = words.filter(val => val.toLowerCase().includes(keyword.toLowerCase()));
            searchResult.push(...matchWords);
            words = removeListOfElements(words, ...matchWords);
        }

        // criteria 4
        if (searchResult.length < 3) {
            const lstGuessKeyword = [];

            for (let i = 0; i < keyword.length; i++) {
                lstGuessKeyword.push((keyword.slice(0, i) + keyword.slice(i + 1, keyword.length)).toLowerCase());
            }

            const matchWords = words.filter(val => lstGuessKeyword.some(guessKw => val.toLowerCase().includes(guessKw)));
            searchResult.push(...matchWords);
            words = removeListOfElements(words, ...matchWords);
        }

        // criteria 5
        if (searchResult.length < 3) {
            const matchWords = words.filter(val => keyword.toLowerCase().includes(val.toLowerCase()))
                .sort((a, b) => b.length - a.length || b.localeCompare(a));
            searchResult.push(...matchWords);
            words = removeListOfElements(words, ...matchWords);
        }

        return searchResult;
    } catch (error) {
        console.log(error);
        return [];
    }
}

/**
 * Replace a selected word by a given one in a file
 * @param {string} selected selected word - will be replaced
 * @param {string} given given word
 * @param {string} fileName written file
 * @returns {object | null}
 */
export async function updateBySelectedWord(selected, given, fileName) {
    try {
        const data = (await readFile(fileName)).toString();
        if (listAllWords(data).includes(selected)) {
            await writeFile(fileName, replaceAll(data, [selected], given));
            return { oldWord: selected, newWord: given };
        }
    } catch (error) {
        console.log(error);
    }
    return null;
}

/**
 * Replace the most similar word by a givenone in a file
 * @param {string} given given word
 * @param {string} fileName written file
 * @returns {object | null}
 */
export async function updateByMostSimilarWord(given, fileName) {
    try {
        const data = (await readFile(fileName)).toString();
        const searchResult = await search(given, fileName);
        const mostSimilarWord = searchResult.length >= 1 ? searchResult[0] : null;

        if (!mostSimilarWord) return null;

        await writeFile(fileName, replaceAll(data, [mostSimilarWord], given));
        return { oldWord: mostSimilarWord, newWord: given };
    } catch (error) {
        console.log(error);
    }
    return null;
}