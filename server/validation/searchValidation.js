import Error from '../helper/error.js';

/**
 * @param {object} data validated data
 * @returns {string[]|null} returned errors
 */

export function searchValidation(data) {
    const error = new Error();

    error.isRequired(data.keyword, "Keyword")
        .isSingleWord(data.keyword, "Keyword");

    return error.get();
}

export function updateValidation(data) {
    const error = new Error();

    error.isSingleWord(data.selected, "Selected word")
        .isRequired(data.givenWord, "Given word")
        .isSingleWord(data.givenWord, "Given word");

    return error.get()
}