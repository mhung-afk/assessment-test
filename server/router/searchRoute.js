import express from 'express';
import { sendError, sendSuccess } from '../helper/client.js';
import { search, updateByMostSimilarWord, updateBySelectedWord } from '../service/searchService.js';
import { searchValidation, updateValidation } from '../validation/searchValidation.js';
import { CORPUS_FILE } from '../constant.js';
import path from "path";
const __dirname = path.resolve(path.dirname(''));

const searchRoute = express.Router();

/**
 * @route GET /api/v1/search
 * @description find the most similar words by given keyword
 * @access public
 */
searchRoute.get('', async (req, res) => {
    const errors = searchValidation(req.query);
    if (errors) return sendError(res, errors);

    const keyword = req.query.keyword.trim();

    try {
        const result = await search(keyword, CORPUS_FILE);
        return sendSuccess(res, "Search successfully.", result.length > 3 ? result.slice(0, 3) : result);
    } catch (error) {
        console.log(error);
        return sendError(res, "Server internal error.", 500);
    }
});

/**
 * @route PUT /api/v1/search
 * @description replace the most similar or the selected word by another
 * @access public
 */
searchRoute.put('', async (req, res) => {
    const updateErrors = updateValidation(req.body);
    if (updateErrors) return sendError(res, updateErrors);

    const [selected, given] = [req.body.selected?.trim(), req.body.givenWord.trim()];

    try {
        let result = null;
        if (selected) {
            result = await updateBySelectedWord(selected, given, CORPUS_FILE);
        }
        else {
            result = await updateByMostSimilarWord(given, CORPUS_FILE);
        }
        if (result) return sendSuccess(res, "Update successfully.", result);
        return sendError(res, "Update failed.")
    } catch (error) {
        console.log(error);
        return sendError(res, "Server internal error.", 500);
    }
});

/**
 * @route GET /api/v1/search/corpus
 * @description download corpus file
 * @access public
 */
searchRoute.get('/corpus', async (req, res) => {
    try {
        res.sendFile(path.join(__dirname, CORPUS_FILE));
    } catch (error) {
        console.log(error)
        return sendError(res, "Get resources failed.");
    }
})

export default searchRoute;