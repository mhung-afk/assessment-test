import express from 'express';
import cors from 'cors'
import searchRoute from './router/searchRoute.js';
import { PORT } from './constant.js';

import path from "path";
const __dirname = path.resolve(path.dirname(''));

const app = express();

app.use(cors())
app.use(express.json());
app.use(express.static(path.join(__dirname, '../client/build')));

app.use('/api/v1/search', searchRoute);

app.get('/*', async (req, res) => {
    try {
        res.sendFile(path.join(__dirname, '../client/build/index.html'))
    } catch (error) {
        console.log(error.message)
        res.sendStatus(500)
    }
})

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`)
});