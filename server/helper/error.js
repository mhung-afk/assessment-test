export default class Error {
    constructor() {
        this.errors = [];
        this.checkRequire = true;
    }

    /**
     * @param {*} field validate field
     * @param {string} name default name is param field's name
     * @returns this
     */
    isRequired(field, name) {
        if (field == null || field == undefined) this.appendError(`${name} field is required.`);
        // if(this.checkRequire)   this.checkRequire = false
        return this;
    }

    /**
     * @param {*} field validate field
     * @param {string} name default name is param field's name
     * @returns this
     */
    isSingleWord(field, name) {
        field = field ? field.trim() : field;
        if (!/^([0-9a-zA-Z]{1,})$/.test(field)) this.appendError(`${name} field isn't a single word.`);
        return this;
    }

    appendError(message) {
        this.errors.push(message);
    }

    get() {
        return this.errors.length > 0 ? this.errors : null;
    }
}