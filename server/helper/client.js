/**
 * Return a success response
 * @param {Response} res response
 * @param {string} message response message
 * @param {number} code success status code
 * @param {object} data response data based on JSON
 * @returns {Response}
 */
export const sendSuccess = (res, message, data = null, code = 200) => {
    let responseJson = {
        message: message
    };
    if (data) responseJson.data = data;
    return res.status(code).json(responseJson);
};

/**
 * Return an error
 * @param {Response} res response
 * @param {string} message error message
 * @param {number} code error status code
 * @returns {Response}
 */
export const sendError = (res, message, code = 400) => {
    return res.status(code).json({
        message: message
    });
};