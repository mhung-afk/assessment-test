import fs from 'fs/promises';

/**
 * @param {string} fileName name of the file that need to be loaded
 * @returns {Buffer} content
 */
export async function readFile(fileName) {
    try {
        return await fs.readFile(fileName);
    } catch (error) {
        console.log(error);
        return null;
    }
}

/**
 * @param {string} fileName name of the file that need to be loaded
 * @param {string} givenData new data for updated file
 * @returns {Buffer} true if write successfuly, otherwise false
 */
export async function writeFile(fileName, givenData) {
    try {
        await fs.writeFile(fileName, givenData, 'utf-8');
        return true;
    } catch (error) {
        console.log(error);
        return false;
    }
}

/**
 * Get list of unique words in a string
 * @param {string} str input string
 * @returns {string[]} array of unique words
 */
export function listAllWords(str) {
    return str.split(/[^0-9a-zA-Z]+/g).filter((val, idx, self) => self.indexOf(val) == idx && val.length > 0);
}

/**
 * @param {string} str input string
 * @param {string[]} words searched strings
 * @param {string} replacement given string for replacing
 * @returns {string} updated string
 */
export function replaceAll(str, words, replacement) {
    const re = new RegExp('\\b' + words.join('|') + '\\b', 'gi');
    return str.replace(re, replacement);
}

/**
 * @param {any[]} origin array need to be removed some elements
 * @param  {...any} elements given elements
 * @returns {any[]} updated array
 */
export function removeListOfElements(origin, ...elements) {
    return origin.filter(val => !elements.includes(val))
}